module.exports = function (grunt) {

    // Project config.
    grunt.initConfig({
        connect: {
            server: {
                options: {
                    livereload: true,
                    port: 9001,
                    hostname: "*",
                    open: {
                        target: "http://127.0.0.1:9001"
                    }
                }
            }
        },
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    // target.css file: source.less file
                    "coltejer-components/coltejer-navbar/coltejer-navbar.min.css": "coltejer-components/coltejer-navbar/coltejer-navbar.less",
                    "coltejer-components/coltejer-table/coltejer-table.min.css": "coltejer-components/coltejer-table/coltejer-table.less",
                    "coltejer-components/coltejer-sidebar-profile/coltejer-sidebar-profile.min.css": "coltejer-components/coltejer-sidebar-profile/coltejer-sidebar-profile.less",
                    "coltejer-components/coltejer-sidebar-menu/coltejer-sidebar-menu.min.css": "coltejer-components/coltejer-sidebar-menu/coltejer-sidebar-menu.less",
                    "coltejer-components/coltejer-top-bar/coltejer-top-bar.min.css": "coltejer-components/coltejer-top-bar/coltejer-top-bar.less",
                    "coltejer-components/coltejer-table-offline/coltejer-table-offline.min.css": "coltejer-components/coltejer-table-offline/coltejer-table-offline.less",
                    "coltejer-components/coltejer-infolink-icon/coltejer-infolink-icon.min.css": "coltejer-components/coltejer-infolink-icon/coltejer-infolink-icon.less",
                    "coltejer-components/coltejer-login/coltejer-login.min.css": "coltejer-components/coltejer-login/coltejer-login.less",

                }
            }
        },
        watch: {
            styles: {
                files: ['**/*.less'], // which files to watch
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            },
            options: {
                livereload: true,
            },
            html: {
                files: ['**/*.html', '*.html'],
            },
            js: {
                files: ['**/*.js'],
                tasks: ['newer:uglify:my_target']
            },
            css: {
                files: ['**/*.css'],
            },
        },
        uglify: {
            options: {
                mangle: false,
                except: ['Angular'],
                sourceMap: true,
                ASCIIOnly: true,
                preserveComments: 'some',
                compress: {
                    drop_console: false
                }
            },
            my_target: {
                files: {
                    "coltejer-components/coltejer-navbar/coltejer-navbar.min.js": "coltejer-components/coltejer-navbar/coltejer-navbar.js",
                    "coltejer-components/coltejer-table/coltejer-table.min.js": "coltejer-components/coltejer-table/coltejer-table.js",
                    "coltejer-components/coltejer-sidebar-profile/coltejer-sidebar-profile.min.js": "coltejer-components/coltejer-sidebar-profile/coltejer-sidebar-profile.js",
                    "coltejer-components/coltejer-sidebar-menu/coltejer-sidebar-menu.min.js": "coltejer-components/coltejer-sidebar-menu/coltejer-sidebar-menu.js",
                    "coltejer-components/coltejer-top-bar/coltejer-top-bar.min.js": "coltejer-components/coltejer-top-bar/coltejer-top-bar.js",
                    "coltejer-components/coltejer-table-offline/coltejer-table-offline.min.js": "coltejer-components/coltejer-table-offline/coltejer-table-offline.js",
                    "coltejer-components/coltejer-infolink-icon/coltejer-infolink-icon.min.js": "coltejer-components/coltejer-infolink-icon/coltejer-infolink-icon.js",
                    "coltejer-components/coltejer-login/coltejer-login.min.js": "coltejer-components/coltejer-login/coltejer-login.js"


                }
            }
        }
    });

    // Actually running things.
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-newer');

    // Default task(s).
    grunt.registerTask('default', ['connect:server', 'watch']);
};