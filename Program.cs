﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CT_ColtejerComponents
{
    class Program
    {
        static void Main(string[] args)
        {
            string cmdNPMBower = @"npm install -g bower";
            //ExecuteCommand(cmdNPMBower);
            string cmdNPM = @"npm install";
            ExecuteCommand(cmdNPM);
            string cmdBower = @"bower install";
            ExecuteCommand(cmdBower);
            string cmdStart = @"grunt";
            ExecuteCommand(cmdStart);
            Environment.Exit(0);
        }

        // Ejecuta comandos BAT
        static void ExecuteCommand(string command)
        {
            string path = new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + "cd " + path + "&&" + command);
            processInfo.CreateNoWindow = false;
            processInfo.UseShellExecute = false;

            process = Process.Start(processInfo);
            process.WaitForExit();

            exitCode = process.ExitCode;

            process.Close();
        }
    }
}
