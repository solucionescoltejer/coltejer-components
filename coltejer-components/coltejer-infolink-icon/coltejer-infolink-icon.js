﻿/**
 * Creado por Andrés Botero, 29/10/2015
 */
angular.module('coltejer.infolink.icon', ['ngResource'])
    .run(function ($templateCache) {
        $templateCache.put('coltejer-infolink-icon.html', ' <div class="text-center"> <a ng-if="hyperlink==\'#\'"> <button class="btn btn-coltejer-infolink btn-success-infolink" ng-class=btnCircleClass style={{extra}}> <i ng-class=glyphiconRealClass>{{materialIcon}}</i> </button> </a> <a ng-if="hyperlink!=\'#\'" href={{hyperlink}}> <button class="btn btn-coltejer-infolink btn-success-infolink" ng-class=btnCircleClass style={{extra}}> <i ng-class=glyphiconRealClass>{{materialIcon}}</i> </button> </a> <h4>{{iconTitle}}</h4> <p>{{briefing}}</p></div>');
    })

    .directive('coltejerInfolinkIcon', function ($templateCache) {
        return {
            restrict: 'EA',
            scope: {
                materialIcon: '@',
                hyperlink: '@',
                briefing: '@',
                iconTitle: '@',
                size: '@',
                extra: '@'
            },
            replace: true,
            template: $templateCache.get('coltejer-infolink-icon.html'),
            link: function (scope, elem, attrs) {
                scope.error = {
                    exist: false,
                    message: ""
                };
                if (scope.size == 'xlg') {
                    scope.glyphiconRealClass = 'material-icons glyphicon-xbig-infolink';
                    scope.btnCircleClass = 'btn-circle-infolink-xlg';
                }
                else if (scope.size == 'lg')
                {
                    scope.glyphiconRealClass = 'material-icons glyphicon-big-infolink';
                    scope.btnCircleClass = 'btn-circle-infolink-lg';
                }
                else if (scope.size == 'xsm') {
                    scope.glyphiconRealClass = 'material-icons glyphicon-xsmall-infolink';
                    scope.btnCircleClass = 'btn-circle-infolink-xsm';
                }
                else if (scope.size == 'md') {
                    scope.glyphiconRealClass = 'material-icons glyphicon-medium-infolink';
                    scope.btnCircleClass = 'btn-circle-infolink-md';
                }
                else
                {
                    scope.glyphiconRealClass = 'material-icons glyphicon-small-infolink';
                    scope.btnCircleClass = 'btn-circle-infolink-sm';
                }
            }
        };
    });