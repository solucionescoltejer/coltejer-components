/**
 * Creado por Andrés Botero, 29/10/2015
 */
angular.module('coltejer.login', ['ngResource', 'ngCookies', 'ngMaterial', 'ngMessages'])
    .run(function ($templateCache) {
        $templateCache.put('coltejer-login.html', '<div> <div layout="row" class="login-div"> <div flex="10" layout-fill></div><div layout-fill class="login-panel"> <md-card> <md-card-title> <md-card-title-text><center> <img class="login-logo img-responsive center-block" src="static/img/favicon.png" alt="Coltejer S.A"/> </center><h4 class="text-center login-title1">{{displayTitle}}</h4> <p class="text-center login-title2">{{displayTitle2}}</p> </md-card-title-text> </md-card-title> <md-content name="content"> <div layout="row" class="campo-login" layout-align="space-between center"> <div flex="30" layout-fill></div><md-input-container class="md-block" flex-gt-sm> <label>Usuario</label> <input required name="NombreUsuario" ng-model="modelUser"> </md-input-container> <div flex="30" layout-fill></div></div><div layout="row" class="campo-login" layout-align="space-between center"> <div flex="30" layout-fill></div><md-input-container class="md-block" flex-gt-sm> <label>Contraseña</label> <input name="Contrasena" required type="password" ng-model="modelPass" ng-keyup="$event.keyCode==13 && realizarAutenticacion()"> </md-input-container> <div flex="30" layout-fill></div></div><div layout="row" class="login-error" ng-show="error==true"><div flex="30" layout-fill></div><p ng-if="mensajeError != null">{{mensajeError}}</p><p ng-if="mensajeError == null">Las credenciales ingresadas, son incorrectas. Por favor, intente de nuevo.</p><div flex="30" layout-fill></div></div><div layout="row" class="campo-login"> <div offset="50" layout-fill> <md-button ng-click="realizarAutenticacion()"> Ingresar </md-button> <md-button  ng-click="cambiarContrasena()">Cambiar Contraseña</md-button> </div></div></md-content> </md-card> </div><div flex="10" layout-fill></div></div></div>');
    })
    .factory('DataSourceLogin', function ($resource) {
        return $resource('http://:server' + ':' + ':port/:context/Login.json', {
            server: '@server',
            port: '@port',
            context: '@context'
        });
    })

    .factory('TokenHandler', function ($cookies) {
        var tokenHandler = {};
        var token = "none";

        tokenHandler.set = function (cookie, newToken, location) {
            if (location == "cookie") {
                $cookies.put(cookie, newToken);
            }
            else {
                window.localStorage.setItem(cookie, newToken);
            }
        };

        tokenHandler.get = function (cookie, location) {
            if (location == "cookie") {
                return $cookies.get(cookie);
            }
            else {
                return window.localStorage.getItem(cookie);
            }

        };

        tokenHandler.remove = function (cookie, location) {
            if (location == "cookie") {
                if ($cookies.get(cookie) != null)
                    $cookies.remove(cookie);
            }
            else {
                if (window.localStorage.getItem(cookie) != null)
                    window.localStorage.removeItem(cookie);
            }
        };

        return tokenHandler;
    })

    .factory('Base64', function () {
        /* jshint ignore:start */

        var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

        return {
            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                do {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);

                return output;
            },

            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                var base64test = /[^A-Za-z0-9\+\/\=]/g;
                if (base64test.exec(input)) {
                    window.alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
                }
                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                do {
                    enc1 = keyStr.indexOf(input.charAt(i++));
                    enc2 = keyStr.indexOf(input.charAt(i++));
                    enc3 = keyStr.indexOf(input.charAt(i++));
                    enc4 = keyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";

                } while (i < input.length);

                return output;
            }
        };

        /* jshint ignore:end */
    })

    .directive('coltejerLogin', function ($templateCache, DataSourceLogin) {
        return {
            restrict: 'EA',
            scope: {
                backServer: '@',
                port: '@',
                context: '@',
                objectName: '@',
                urlRedirect: '@',
                cookieName: '@',
                displayTitle: '@',
                displayTitle2: '@',
                tokenLocation: '@',
                urlContrasena:'@'
            },
            replace: true,
            template: $templateCache.get('coltejer-login.html'),
            link: function (scope, elem, attrs) {
                scope.error = {
                    exist: false,
                    message: ""
                };

            },
            controller: function ($scope, $window, DataSourceLogin, $location, Base64, $cookies, $mdDialog, TokenHandler) {
                $scope.error = false;
                  $scope.cambiarContrasena = function () {
                    window.location.replace('http://fenix.coltejer.loc:3049/#/Home/'+ $scope.urlContrasena);
                      //;
                  }
                $scope.realizarAutenticacion = function () {
                    var dialogo = $mdDialog.alert()
                    dialogo
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(false)
                    .title('Autenticando')
                    .textContent('Realizando autenticación, por favor espere.')
                    .ariaLabel('Información');
                    $mdDialog.show(dialogo);

                    $scope.modelUser = $scope.modelUser.toLowerCase().trim();
                    $scope.credencialesEncriptadas = Base64.encode($scope.modelUser + ':' + $scope.modelPass);
                    $scope.data = new DataSourceLogin({ DatosUsuario: { Credenciales: $scope.credencialesEncriptadas } });
                    $scope.data.$save({
                        server: $scope.backServer,
                        port: $scope.port,
                        context: $scope.context
                    },
                    function (data) {
                        $mdDialog.cancel(dialogo);
                        $scope.respuesta = data[$scope.objectName];
                        if ($scope.respuesta == true) {
                            if (TokenHandler.get($scope.cookieName, $scope.tokenLocation) == null)
                            {
                                TokenHandler.set($scope.cookieName, data['Token'], $scope.tokenLocation);
                            }

                            $location.path($scope.urlRedirect);
                        }
                        else {
                            //Malas credenciales
                            $scope.error = true;
                            $scope.mensajeError = data['Error'];
                        }
                    },
                    function (err) {
                        $mdDialog.cancel(dialogo);
                        $scope.error = true;
                    });
                }
            }
        };
    });
