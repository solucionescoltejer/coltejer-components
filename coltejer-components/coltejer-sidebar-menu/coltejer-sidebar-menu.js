﻿/**
 * Creado por Santiago Franco, 06/01/2016
 */
angular.module('coltejer.sidebar.menu', ['ngResource'])
    .run(function ($templateCache) {
        $templateCache.put('coltejer-sidebar-menu.html', '<div ng-app="coltejer.sidebar.menu" class="send-to-top-1"> <md-button class="md-primary coltejer-sidebar-menu-btn-deploy send-to-top-2" ng-hide="isOpenLeft()" aria-label="Menu" ng-click="toggleLeft()" ng-if="sidebarSide==\'left\'"> <md-icon md-svg-src="static/img/icons/menu.svg" class="menu"></md-icon> </md-button> <md-button class="md-primary coltejer-sidebar-menu-btn-deploy send-to-top-2" ng-hide="isOpenRight()" aria-label="Menu" ng-click="toggleRight()" ng-if="sidebarSide==\'right\'"> <md-icon md-svg-src="static/img/icons/menu.svg" class="menu"></md-icon> </md-button> <md-sidenav class="md-sidenav-right md-whiteframe-z2 fixed" is-locked-open="$media(\'\')" style="height: calc(100vh -{{alturaNavbar}})" md-component-id="right" ng-if="sidebarSide==\'right\'"> <md-toolbar class="md-theme-light"> <h3 class="md-toolbar-tools">Menú</h3> </md-toolbar> <md-content layout-padding=""> <md-list> <md-list-item ng-if="error.exist"> <p>{{error.message}}</p><md-icon md-font-icon="android" aria-label="android"> </md-list-item> <md-list-item class="menu-box" ng-repeat="option in menu" ng-if="option.SubMenu==null"> <a class="menu-link" href="{{option.Uri}}">{{option.Titulo}}</a> <md-icon md-font-icon="android" aria-label="android"> </md-list-item> <md-menu md-offset="0 70" ng-repeat="option in menu" ng-if="option.SubMenu !=null"> <md-list-item ng-click="$mdOpenMenu($event)"> <a class="menu-link" href="#">{{option.Titulo}}</a> </md-list-item> <md-menu-content width="4"> <md-menu-item ng-repeat="suboption in option.SubMenu"> <a class="menu-link" href="{{suboption.Uri}}"> <i class={{suboption.Icon}}></i>{{suboption.Titulo}}</a> </md-menu-item> </md-menu-content> </md-menu> </md-list> <md-button ng-click="close()" class="md-primary"> Cerrar </md-button> </md-content> </md-sidenav> <md-sidenav class="md-sidenav-left md-whiteframe-z2 fixed" is-locked-open="$media(\'\')" style="height: calc(100vh -{{alturaNavbar}})" md-component-id="left" ng-if="sidebarSide==\'left\'"> <md-toolbar class="md-theme-light"> <h3 class="md-toolbar-tools">Menú</h3> </md-toolbar> <md-content layout-padding=""> <md-list> <md-list-item ng-if="error.exist"> <p>{{error.message}}</p><md-icon md-font-icon="android" aria-label="android"> </md-list-item> <md-list-item class="menu-box" ng-repeat="option in menu" ng-if="option.SubMenu==null"> <a class="menu-link" href="{{option.Uri}}">{{option.Titulo}}</a> <md-icon md-font-icon="android" aria-label="android"> </md-list-item> <md-menu md-offset="0 70" ng-repeat="option in menu" ng-if="option.SubMenu !=null"> <md-list-item ng-click="$mdOpenMenu($event)"> <a class="menu-link" href="#">{{option.Titulo}}</a> </md-list-item> <md-menu-content width="4"> <md-menu-item class="menu-box" ng-repeat="suboption in option.SubMenu"> <a class="menu-link" href="{{suboption.Uri}}"> <i class={{suboption.Icon}}></i>{{suboption.Titulo}}</a> </md-menu-item> </md-menu-content> </md-menu> </md-list> <md-button ng-click="close()" class="md-primary"> Cerrar </md-button> </md-content> </md-sidenav></div>');
    })
    .factory('DataSourceMenu', function ($resource) {
        return $resource('http://:server' + ':' + ':port/:context/menu.json', {
            server: '@server',
            port: '@port',
            context: '@context'
        });
    })


    .directive('coltejerSidebarMenu', function (DataSourceMenu, $templateCache, $timeout, $mdSidenav, $log) {
        return {
            restrict: 'EA',
            scope: {
                backServer: '@',
                frontServer: '@',
                port: '@',
                context: '@',
                objectName: '@',
                iniUri: '@',
                logoPath: '@',
                hasLogout: '@',
                alturaNavbar: '@',
                sidebarSide: '@'
            },
            replace: true,
            template: $templateCache.get('coltejer-sidebar-menu.html'),
            link: function (scope, elem, attrs) {
                scope.error = {
                    exist: false,
                    message: ""
                };
                scope.menu = DataSourceMenu.get({
                    server: scope.backServer,
                    port: scope.port,
                    context: scope.context
                },
                    function (menu) {
                        scope.menu = menu[scope.objectName];
                    },
                    function (err) {;
                        scope.menu.IniUri = "http://" + scope.frontServer + ":" + scope.port + "/" + scope.context;
                        scope.error.exist = true;
                        if (err.statusText == "") {
                            scope.error.message = "Hubo un error recopilando los datos de usuario";
                        } else {
                            scope.error.message = "Hubo un error recopilando los datos de usuario: " + err.statusText;
                        }
                    });

            },

            controller: function ($scope, $timeout, $mdSidenav, $log) {
                $scope.toggleLeft = buildToggler('left');
                $scope.toggleRight = buildToggler('right');
                $scope.isOpenLeft = function () {
                    return $mdSidenav('left').isOpen();
                };
                $scope.isOpenRight = function () {
                    return $mdSidenav('right').isOpen();
                };
                /**
            * Supplies a function that will continue to operate until the
            * time is up.
            */
                function debounce(func, wait, context) {
                    var timer;
                    return function debounced() {
                        var context = $scope,
                            args = Array.prototype.slice.call(arguments);
                        $timeout.cancel(timer);
                        timer = $timeout(function () {
                            timer = undefined;
                            func.apply(context, args);
                        }, wait || 10);
                    };
                }
                /**
            * Build handler to open/close a SideNav; when animation finishes
            * report completion in console
            */
                function buildDelayedToggler(navID) {
                    return debounce(function () {
                        $scope.alturaNavbar = '0px';
                        $mdSidenav(navID)
                          .toggle()
                          .then(function () {
                              $log.debug("toggle " + navID + " is done");
                          });
                    }, 200);
                }

                function buildToggler(navID) {
                    return function () {
                        $mdSidenav(navID)
                          .toggle()
                          .then(function () {
                              $log.debug("toggle " + navID + " is done");
                          });
                    }
                }

                $scope.close = function () {
                    if ($scope.sidebarSide == 'left') {
                        $mdSidenav('left').close()
  .then(function () {
      $log.debug("close LEFT is done");
  });
                    }

                    else if ($scope.sidebarSide == 'right') {
                        $mdSidenav('right').close()
  .then(function () {
      $log.debug("close RIGHT is done");
  });
                    }

                };

                var originatorEv;
                $scope.openMenu = function ($mdOpenMenu, ev) {
                    originatorEv = ev;
                    $mdOpenMenu(ev);
                };
            }
        };
    });