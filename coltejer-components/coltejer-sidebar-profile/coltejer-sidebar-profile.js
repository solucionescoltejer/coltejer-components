﻿/**
 * Creado por Santiago Franco, 06/01/2016
 */
angular.module('coltejer.sidebar.profile', ['ngResource'])
    .run(function ($templateCache) {
        $templateCache.put('coltejer-sidebar-profile.html', '<div ng-app="coltejer.sidebar.profile" class="send-to-top-1"> <md-button class="md-icon-button coltejer-sidebar-btn-deploy-left send-to-top-1" ng-hide="isOpenLeft()" aria-label="Menu" ng-click="toggleLeft()" ng-if="sidebarSide==\'left\'"> <md-icon md-svg-src="static/img/icons/avatar.svg" class="menu"></md-icon> </md-button> <md-button class="md-icon-button coltejer-sidebar-btn-deploy-right send-to-top-1" ng-hide="isOpenRight()" aria-label="Menu" ng-click="toggleRight()" ng-if="sidebarSide==\'right\'"> <md-icon md-svg-src="static/img/icons/avatar.svg" class="menu"></md-icon> </md-button> <md-sidenav class="md-sidenav-left md-whiteframe-z1 fixed" style="height: 100vh" md-component-id="left"  ng-if="sidebarSide==\'left\'"> <md-toolbar layout-padding="" class="coltejer-sidebar-center"> <div> <md-icon md-svg-src="static/img/icons/avatar.svg" class="avatar icon-md"></md-icon> <p>{{datosUsuario.Nombre}}</p></div></md-toolbar> <md-content layout-padding=""> <p ng-if=error.exist>{{error.message}}</p><p ng-repeat="(key,val) in datosUsuario">{{key}}: {{val}}</p><md-button ng-click="close()" class="md-primary"> Cerrar </md-button> </md-content> </md-sidenav> <md-sidenav class="md-sidenav-right md-whiteframe-z1 no-static-profile-right fixed" style="height: 100vh" md-component-id="right" ng-if="sidebarSide==\'right\'"> <md-toolbar layout-padding="" class="coltejer-sidebar-center"> <div> <md-icon md-svg-src="static/img/icons/avatar.svg" class="avatar icon-md"></md-icon> <p>{{datosUsuario.Nombre}}</p></div></md-toolbar> <md-content layout-padding=""> <p ng-if=error.exist>{{error.message}}</p><p ng-repeat="(key,val) in datosUsuario">{{key}}: {{val}}</p><md-button ng-click="close()" class="md-primary"> Cerrar </md-button> </md-content> </md-sidenav></div>');
    })
    .factory('DatosUsuario', function ($resource) {
        return $resource('http://:server' + ':' + ':port/:context/empleado/:usuario.json', {
            server: '@server',
            port: '@port',
            context: '@context',
            usuario: '@usuario'
        });
    })


    .directive('coltejerSidebarProfile', function (DatosUsuario, $templateCache, $timeout, $mdSidenav, $log) {
        return {
            restrict: 'EA',
            scope: {
                backServer: '@',
                frontServer: '@',
                port: '@',
                context: '@',
                objectName: '@',
                iniUri: '@',
                logoPath: '@',
                hasLogout: '@',
                avoidElements: '@',
                alturaNavbar: '@',
                user: '@',
                sidebarSide: '@'
            },
            replace: true,
            template: $templateCache.get('coltejer-sidebar-profile.html'),
            link: function (scope, elem, attrs) {
                scope.error = {
                    exist: false,
                    message: ""
                };
                scope.datosUsuario = DatosUsuario.get({
                    server: scope.backServer,
                    port: scope.port,
                    context: scope.context,
                    usuario: scope.user
                },
                    function (datosUsuario) {
                        scope.avoidElements2 = [];
                        scope.avoidElements2 = scope.avoidElements.split(',');
                        scope.datosUsuario = datosUsuario[scope.objectName];
                        for (var i = 0; i < scope.avoidElements2.length; i++) {
                            angular.forEach(scope.datosUsuario, function (value, key) {
                                if (key == scope.avoidElements2[i]) {
                                    delete scope.datosUsuario[key];
                                }
                            });
                        }
                    },
                    function (err) {;
                        scope.datosUsuario.IniUri = "http://" + scope.frontServer + ":" + scope.port + "/" + scope.context;
                        scope.error.exist = true;
                        if (err.statusText == "") {
                            scope.error.message = "Hubo un error recopilando los datos de usuario";
                        } else {
                            scope.error.message = "Hubo un error recopilando los datos de usuario: " + err.statusText;
                        }
                    });

            },

            controller: function ($scope, $timeout, $mdSidenav, $log) {
                $scope.toggleLeft = buildDelayedToggler('left');
                $scope.toggleRight = buildDelayedToggler('right');
                $scope.isOpenLeft = function () {
                    return $mdSidenav('left').isOpen();
                };
                $scope.isOpenRight = function () {
                    return $mdSidenav('right').isOpen();
                };
                /**
            * Supplies a function that will continue to operate until the
            * time is up.
            */
                function debounce(func, wait, context) {
                    var timer;
                    return function debounced() {
                        var context = $scope,
                            args = Array.prototype.slice.call(arguments);
                        $timeout.cancel(timer);
                        timer = $timeout(function () {
                            timer = undefined;
                            func.apply(context, args);
                        }, wait || 10);
                    };
                }
                /**
            * Build handler to open/close a SideNav; when animation finishes
            * report completion in console
            */
                function buildDelayedToggler(navID) {
                    return debounce(function () {
                        $mdSidenav(navID)
                          .toggle()
                          .then(function () {
                              $log.debug("toggle " + navID + " is done");
                          });
                    }, 200);
                }

                function buildToggler(navID) {
                    return function () {
                        $mdSidenav(navID)
                          .toggle()
                          .then(function () {
                              $log.debug("toggle " + navID + " is done");
                          });
                    }
                }

                $scope.close = function () {
                    if ($scope.sidebarSide == 'left') {
                        $mdSidenav('left').close()
  .then(function () {
      $log.debug("close LEFT is done");
  });
                    }

                    else if ($scope.sidebarSide == 'right') {
                        $mdSidenav('right').close()
  .then(function () {
      $log.debug("close RIGHT is done");
  });
                    }

                };
            }
        };
    });