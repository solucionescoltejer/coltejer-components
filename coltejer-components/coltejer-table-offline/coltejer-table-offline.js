﻿/**
 * Creado por Andrés Botero, 29/10/2015
 */
angular.module('coltejer.table.offline', ['ngResource'])
    .run(function ($templateCache) {
        $templateCache.put('coltejer-table-offline.html', '<div class="coltejer-responsive-table-offline" ng-app="coltejer.table"> <table class="col-md-12 table-bordered table-striped table-condensed cf coltejer-unmargined-table-offline"> <thead class=cf> <tr> <th ng-repeat="field in fields" class=text-center>{{field}}</th> </tr> </thead> <tbody><tr ng-repeat="object in data  | filter:filter"> <td ng-repeat="field in fields" data-title={{field}}>{{object[field]}}</td> </tr> </tbody> </table> </div>');
    })
    .directive('coltejerTableOffline', function (DataSource, $templateCache) {
        return {
            restrict: 'EA',
            scope: {
                listName: '=',
                avoidElements: '@',
                filter: '='
            },
            replace: true,
            template: $templateCache.get('coltejer-table-offline.html'),
            link: function (scope, elem, attrs) {
                scope.error = {
                    exist: false,
                    message: ""
                };
                if (scope.listName != null) {
                    scope.fields = [];
                    scope.object = scope.listName[0];
                    if (scope.avoidElements != null) {
                        scope.avoidElements2 = [];
                        scope.avoidElements2 = scope.avoidElements.split(',');
                        for (var propertyName in scope.object) {
                            var existe = false;
                            for (var i = 0; i < scope.avoidElements2.length; i++) {
                                if (propertyName == scope.avoidElements2[i]) {
                                    existe = true;
                                }
                            }
                            if (!existe) {
                                scope.fields.push(propertyName);
                                existe = false;
                            }
                        }
                    }
                    else {
                        for (var propertyName in scope.object) {
                            scope.fields.push(propertyName);
                        }
                    }


                    scope.data = scope.listName;
                }

                scope.$watch('listName', function () {
                    renderizar();
                })
                /*scope.$watch('filter', function () {
                    renderizar();
                })*/

                var renderizar = function ()
                {
                    if (scope.listName != null) {
                        scope.fields = [];
                        scope.object = scope.listName[0];
                        if (scope.avoidElements != null) {
                            scope.avoidElements2 = [];
                            scope.avoidElements2 = scope.avoidElements.split(',');
                            for (var propertyName in scope.object) {
                                var existe = false;
                                for (var i = 0; i < scope.avoidElements2.length; i++) {
                                    if (propertyName == scope.avoidElements2[i]) {
                                        existe = true;
                                    }
                                }
                                if (!existe) {
                                    scope.fields.push(propertyName);
                                    existe = false;
                                }
                            }
                        }
                        else {
                            for (var propertyName in scope.object) {
                                scope.fields.push(propertyName);
                            }
                        }
                        scope.data = scope.listName;
                    }
                }
            }
        };
    });