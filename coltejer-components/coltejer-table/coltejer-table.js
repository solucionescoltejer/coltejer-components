/**
 * Creado por Santiago Franco, 29/10/2015
 */
angular.module('coltejer.table', ['ngResource'])
    .run(function ($templateCache) {
        $templateCache.put('coltejer-table.html', '<div class="coltejer-responsive-table" ng-app="coltejer.table"> <table ng-if="fields.length > 0" class="col-md-12 table-bordered table-striped table-condensed cf coltejer-unmargined-table"> <thead class=cf> <tr> <th ng-repeat="field in fields" class=text-center>{{field}}</th> </tr> </thead> <tbody><tr ng-repeat="object in data | filter:filter"> <td class=text-center ng-repeat="field in fields" data-title={{field}} ng-bind-html="safe(object[field])"></td> </tr> </tbody> </table> </div>');
    })
    .factory('DataSourceTable', function ($resource) {
        return $resource('http://:server' + ':' + ':port/:context/:filters.json', {
            server: '@server',
            port: '@port',
            context: '@context',
            filters: '@filters'
        });
    })
    .directive('coltejerTable', function (DataSourceTable, $templateCache, $sce, $compile) {
        return {
            restrict: 'EA',
            scope: {
                backServer: '@',
                frontServer: '@',
                port: '@',
                context: '@',
                filters: '@',
                avoidElements: '@',
                objectName: '@',
                filter: '='

            },
            replace: true,
            template: $templateCache.get('coltejer-table.html'),
            link: function (scope, elem, attrs) {

                scope.error = {
                    exist: false,
                    message: ""
                };

                scope.data = DataSourceTable.get({
                    server: scope.backServer,
                    port: scope.port,
                    context: scope.context,
                    filters: scope.filters
                },
                    function (data) {
                        scope.fields = [];
                        scope.object = data[scope.objectName][0];

                        if (scope.avoidElements != null) {
                            scope.avoidElements2 = [];
                            scope.avoidElements2 = scope.avoidElements.split(',');
                            for (var propertyName in scope.object) {
                                var existe = false;
                                for (var i = 0; i < scope.avoidElements2.length; i++) {
                                    if (propertyName == scope.avoidElements2[i]) {
                                        existe = true;
                                    }
                                }
                                if (!existe) {
                                    scope.fields.push(propertyName);
                                    // Trust html to be able to render in the view
                                    //scope.object = $sce.trustAsHtml(scope.object.toString());
                                    existe = false;
                                }
                            }
                        }
                        else {
                            for (var propertyName in scope.object) {
                                scope.fields.push(propertyName);
                                // Trust html to be able to render in the view
                               //scope.object = $sce.trustAsHtml(scope.object.toString());
                            }
                        }
                        scope.data = data[scope.objectName];

                        scope.safe = function (val)
                        {
                            return $sce.trustAsHtml(val.toString());
                        }
                    },
                    function (err) {
                        console.log(err);
                        scope.error.exist = true;
                        if (err.statusText == "") {
                            scope.error.message = "Hubo un error cargando la tabla: Código 0, error desconocido";
                        } else {
                            scope.error.message = "Hubo un error cargando la tabla: " + err.statusText;
                        }
                    });
            }
        };
    });