﻿/**
 * Creado por Santiago Franco, 06/01/2016
 */
angular.module('coltejer.top.bar', ['ngResource'])
    .run(function ($templateCache) {
        $templateCache.put('coltejer-top-bar.html', '<md-toolbar layout="row" class="md-whiteframe-z2 fixed"> <div class="md-toolbar-tools"> <a href="#" class="coltejer-bar-top-padding"> <img src="{{iconPath}}" alt="Coltejer S.A" class="coltejer-top-bar-logo"> </a> <span flex></span> </div></md-toolbar>');
    })


    .directive('coltejerTopBar', function ($templateCache) {
        return {
            restrict: 'EA',
            scope: {
                iconPath: '@'
            },
            replace: true,
            template: $templateCache.get('coltejer-top-bar.html')
        };
    });
