# Componentes Web de Coltejer

coltejer-components es un proyecto interno de [AngularJS](https://angularjs.org) que contiene un conjunto de elementos reutilizables de front-end para su uso en aplicaciones web de Coltejer.

## Instalación
Para utilizar un coltejer-component se debe agregar este repositorio, con el usuario personal de bitbucket al bower.json del proyecto web donde se va a utilizar:

```javascript
{
  "name": "mi-proyecto",
  "version": "0.0.1",
  "main": "path/to/main.css",
  "ignore": [
    ".jshintrc",
    "**/*.txt"
  ],
  "dependencies": {
    "angular": "1.3.X",
    "angular-resource": "1.3.X",
    "bootstrap": "3.X.X",
    "bootswatch-dist": "paper"
    "coltejer-components": "https://miusuario@bitbucket.org/solucionescoltejer/coltejer-components.git"
  }
}
```

Luego se ejecuta el instalador de paquetes de Bower sobre el proyecto

```batch
bower install
```

Al terminar, la estructura del proyecto será parecida a esta:
> * bower.json
> * .bowerrc
> * package.json
> * Gruntfile.js
> * .gitignore
> * .gitattributes
> * bower.json
> * node_modules
> * layout.html
> * appname
> > * css
> > * fonts
> > * img
> > * lib
> > > * coltejer-components
> > > > * coltejer-navbar
> > > > > * coltejer-navbar.min.js
> > > > > * coltejer-navbar.min.css
> > * src
> > > * app.js


## Uso
Una vez instalada la librería, se deben importar los archivos en el html que va a utilizarlos, para la mayoría de los proyectos de Coltejer, existe un layout.html donde se encuentra el código principal HTML, allí se importan de este modo:

```html
<!DOCTYPE html>
<html lang="es" ng-app="coltejer">

<head>
    <meta charset="UTF-8">
    <title>Componentes Web Coltejer</title>
    ...
    <link rel="stylesheet" href="appname/lib/coltejer-components/coltejer-navbar/coltejer-navbar.min.css">
</head>
.
.
.
<coltejer-navbar back-server="localhost" front-server="localhost" port="8445" context="prueba" object-name="Menu" logo-path="webapp/img/logo.png">
    </coltejer-navbar>
.
.
.
<script src="appname/lib/coltejer-components/coltejer-components.js"></script>
<script src="appname/lib/coltejer-components/coltejer-navbar/coltejer-navbar.min.js"></script>
```

El anterior ejemplo despliega un html en blanco con la Navbar de Coltejer, cargando el menú desde http://localhost:8445/prueba/menu.json, ruta que devuelve un JSON cuyo nombre es "Menu", además busca la imagen de branding en la ruta "webapp/img/logo.png".

## Componentes
A continuación encontrará una lista de los componentes de la librería, y sus parámetros de uso:

## Navbar
Es la barra superior de la página, en la cual se contiene el menú de opciones, el menú de cierre de sesión, etc.

| Parametro  | Tipo     | Descripción
|------------|----------|-------------
|back-server | *string* | Nombre del servidor donde se encuentra alojado el Backend.
|front-server| *string* | Nombre del servidor donde se encuentra alojado el Frontend.
|port        | *string* | Puerto en el que se encuentra la aplicación, si es el 80 se deja vacío
|context     | *string* | Directorio virtual de la aplicación
|object-name | *string* | Nombre del objeto que vuelve del Backend con el JSON del menú
|logo-path   | *string* | Es la ruta **virtual** donde se encuentra la imagen del logo usada en el brand de la barra, virtural significa que si se está sirviendo con UltiDev Web Server la aplicación con el directorio virtual "prueba", entonces la ruta es: ```"prueba/img/logo.png"```

## Licencia
Este código pertenece a [Coltejer S.A](http://www.coltejer.com.co). Copyright &copy; 2015 Coltejer S.A