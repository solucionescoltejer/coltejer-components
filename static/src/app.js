angular.module('CT_ColtejerComponents', ['coltejer.components', 'ngMaterial', 'ngMessages'])
    .config(function ($mdThemingProvider) {
        $mdThemingProvider.definePalette('amazingPaletteName', {
            '50': '246A4C',
            '100': '246A4C',
            '200': '246A4C',
            '300': '246A4C',
            '400': '246A4C',
            '500': '307B5B',
            '600': '307B5B',
            '700': '307B5B',
            '800': '307B5B',
            '900': '307B5B',
            'A100': '246A4C',
            'A200': '307B5B',
            'A400': '307B5B',
            'A700': '307B5B',
            'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
            // on this palette should be dark or light
            'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
             '200', '300', '400', 'A100'],
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        });
        $mdThemingProvider.theme('default')
          .primaryPalette('amazingPaletteName')

                           
    })